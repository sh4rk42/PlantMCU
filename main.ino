#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#include "WiFiManager.h"
#include "PlantConfigProvider.h"
#include "MQTTManager.h"
#include "Plant.h"
#include "ConfigManager.h"
#include "ADC.h"

WiFiClient espClient;
PubSubClient client(espClient);
WiFiManager* wiFiManager;
ConfigManager configManager;
MQTTManager mqttManager;
Plant** plants = new Plant*[8];
uint8_t numberOfPlants;
ADC adc;

void setup() {
  Serial.begin(115200);
  EEPROM.begin(64);

  WiFiManager _wiFiManager = WiFiManager(&configManager);
  wiFiManager = &_wiFiManager;
  wiFiManager->connect();

  randomSeed(micros());

  PlantConfigProvider plantConfigProvider = PlantConfigProvider(&configManager);
  numberOfPlants = plantConfigProvider.getAll(plants, 8, &mqttManager);

  mqttManager.setup(&configManager, plants, numberOfPlants, client);
}

void loop() {
  mqttManager.loop();

  for(uint8_t i = 0; i < numberOfPlants; i++) {
    plants[i]->loop();
  }
}
