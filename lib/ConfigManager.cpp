#include <FS.h>
#include <WString.h> // for __FlashStringHelper
#include "ConfigManager.h"

ConfigManager::ConfigManager() {
  bool canMount = SPIFFS.begin();
  if(!canMount) {
    Serial.println(F("ConfigManager: unable to mount"));
  }
}

bool ConfigManager::getChar(char *buf, size_t len, const __FlashStringHelper* key) {
  char path[16] = { '\0' };
  strncpy(path, "/cfg/", 5);
  strncat_P(path, (PGM_P)key, 11);

  File file = SPIFFS.open(path, "r");

  if(!file) {
    return false;
  }

  memset(buf, 0, len);
  file.readBytes(buf, len-1);

  return true;
}
