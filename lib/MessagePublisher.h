#ifndef MessagePublisher_h
#define MessagePublisher_h

#include <WString.h> // for __FlashStringHelper

class MessagePublisher {
  public:
    virtual void publish(char* name, const __FlashStringHelper* action, char* payload);
};

#endif