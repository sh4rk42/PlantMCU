#include "ConfigManager.h"
#include "MQTTManager.h"

Plant** MQTTManager::_plants = NULL;
uint8_t MQTTManager::_numberOfPlants = 0;

MQTTManager::MQTTManager() {
}

void MQTTManager::setup(ConfigManager* configManager, Plant** plants, uint8_t numberOfPlants, PubSubClient mqtt) {
  _configManager = configManager;
  _plants = plants;
  _numberOfPlants = numberOfPlants;
  _mqtt = mqtt;

  char server[32];
  bool result = _configManager->getChar(server, sizeof(server), F("MQTT_ADDR"));
  result = result && _configManager->getChar(_user, sizeof(_user), F("MQTT_USER"));
  result = result && _configManager->getChar(_pass, sizeof(_pass), F("MQTT_PASS"));

  if(result) {
    _mqtt.setServer(server, 1883);
    _mqtt.setCallback(MQTTManager::mqttCallback);
  } else {
    Serial.println(F("MQTTManager MQTT_ADDR, MQTT_USER or MQTT_PASS not found"));
  }
}

void MQTTManager::loop() {
  if(!_mqtt.connected()) {
    _reconnect();
    _subscribe();
  }
  _mqtt.loop();
}

void MQTTManager::publish(char* name, const __FlashStringHelper* action, char* payload) {
  char topic[64] = { '\0' };
  char myAction[16] = { '\0' };
  strncpy_P(myAction, (PGM_P)action, 15);
  sprintf_P(topic, PSTR("things/%s/%s/res"), name, myAction);

  Serial.print("MQTTManager publish ");
  Serial.print(topic);
  Serial.print(": ");
  Serial.println(payload);

  _mqtt.publish(topic, payload);
}

void MQTTManager::_reconnect() {
  while(!_mqtt.connected()) {
    String clientId = F("PlantMCU-");
    clientId += String(random(0xffff), HEX);

    if(_mqtt.connect(clientId.c_str(), _user, _pass)) {
      Serial.println(F("MQTTManager connected"));
    } else {
      Serial.println(F("MQTTManager reconnect failed"));
      Serial.println(_mqtt.state());
      delay(5000);
    }
  }
}

void MQTTManager::_subscribe() {
  char topic[64];

  for(uint8_t i = 0; i < _numberOfPlants; i++) {
    Plant* plant = _plants[i];

    sprintf_P(topic, PSTR("things/%s/pumpState/req"), plant->getName());
    _mqtt.subscribe(topic);

    sprintf_P(topic, PSTR("things/%s/rstWaterLvl/req"), plant->getName());
    _mqtt.subscribe(topic);
  }
}

void MQTTManager::mqttCallback(char* topic, byte* payload, unsigned int length) {
  char myPayload[64] = { '\0' };
  if(length > 63) {
    strncpy(myPayload, (char*)payload, 63);
  } else {
    strncpy(myPayload, (char*)payload, length);
  }

  Serial.print(F("MQTTManager receive on "));
  Serial.print(topic);
  Serial.print(F(": "));
  Serial.println(myPayload);

  char *topicToken, *topicSaved;
  uint8_t topicCounter = 0;
  topicToken = strtok_r(topic, "/", &topicSaved);
  Plant* currentPlant = NULL;
  char action[16] = { '\0' };

  while(topicToken != NULL) {
    if(topicCounter == 0 && strcmp_P(topicToken, PSTR("things")) != 0) {
      break;
    } else if(topicCounter == 1) {
      for(uint8_t i = 0; i < _numberOfPlants; i++) {
        if(strcmp(topicToken, _plants[i]->getName()) == 0) {
          currentPlant = _plants[i];
          break;
        }
      }
    } else if(topicCounter > 1 && currentPlant == NULL) {
      break;
    } else if(topicCounter == 2) {
      strncpy(action, topicToken, 15);
    }

    topicToken = strtok_r(NULL, "/", &topicSaved);
    topicCounter += 1;
  }

  if(currentPlant == NULL) {
    Serial.println(F("MQTTManager unknown plant"));
    return;
  }

  if(strcmp_P(action, PSTR("pumpState")) == 0) {
    uint8_t seconds = atoi(myPayload);
    currentPlant->handlePumpStateMessage(seconds);
  } else if(strcmp_P(action, PSTR("rstWaterLvl")) == 0) {
    currentPlant->handleRstWaterLvlMessage();
  } else {
    Serial.print(F("MQTTManager unknown action "));
    Serial.println(action);
    return;
  }

  Serial.print(F("MQTTManager handled action "));
  Serial.println(action);
}
