#ifndef MQTTManager_h
#define MQTTManager_h

#include <WString.h> // for __FlashStringHelper
#include <Arduino.h>
#include <PubSubClient.h>
#include "ConfigManager.h"
#include "PlantConfigProvider.h"

class MQTTManager: public MessagePublisher {
  public:
    MQTTManager();
    void setup(ConfigManager* configManager, Plant** plants, uint8_t numberOfPlants, PubSubClient mqtt);
    void loop();
    void publish(char* name, const __FlashStringHelper* action, char* payload);
    static void mqttCallback(char* topic, byte* payload, unsigned int length);

    static Plant** _plants;
    static uint8_t _numberOfPlants;

  private:
    void _reconnect();
    void _subscribe();
    char _user[32];
    char _pass[32];
    ConfigManager* _configManager;
    PubSubClient _mqtt;
};

#endif
