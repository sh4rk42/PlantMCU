#ifndef ConfigManager_h
#define ConfigManager_h

#include <WString.h> // for __FlashStringHelper

class ConfigManager {
  public:
    ConfigManager();
    bool getChar(char *buf, size_t len, const __FlashStringHelper* key);
};

#endif
