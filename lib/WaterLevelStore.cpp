#include <EEPROM.h>
#include "WaterLevelStore.h"

WaterLevelStore::WaterLevelStore() {
}

void WaterLevelStore::setup(uint8_t pumpNumber) {
  _pumpNumber = pumpNumber;
}

uint8_t WaterLevelStore::getSeconds() {
  uint8_t addr = _pumpNumber;
  uint8_t value = EEPROM.read(addr);
  return value;
}

void WaterLevelStore::putSeconds(uint8_t seconds) {
  uint8_t addr = _pumpNumber;
  EEPROM.write(addr, seconds);
  EEPROM.commit();
}
