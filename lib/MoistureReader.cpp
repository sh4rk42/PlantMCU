#include "MoistureReader.h"

MoistureReader::MoistureReader() {
}

void MoistureReader::setup(uint8_t channel) {
  _channel = channel;
}

float MoistureReader::read() {
  int val = _adc.read(_channel);

  if(val == 0 || val > 1023) {
    return -1;
  }
  return val / 1023.0;
}