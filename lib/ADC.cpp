#include "ADC.h"

#define CS_PIN D0
#define MOSI_PIN D1
#define MISO_PIN D2
#define CLOCK_PIN D3

ADC::ADC() {
  pinMode(PLANT_SENSOR_POWER_PIN, OUTPUT);
}

int ADC::read(uint8_t chan) {
  digitalWrite(PLANT_SENSOR_POWER_PIN, HIGH);
  delay(50);

  MCP3008 adc(CLOCK_PIN, MOSI_PIN, MISO_PIN, CS_PIN);
  int value = adc.readADC(chan);

  digitalWrite(PLANT_SENSOR_POWER_PIN, LOW);
  return value;
}