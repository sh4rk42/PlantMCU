#ifndef Plant_h
#define Plant_h

#include <stdint.h> # for uint8_t
#include "PumpManager.h"
#include "WaterLevelStore.h"
#include "MessagePublisher.h"
#include "PumpManagerDelegate.h"
#include "MoistureReader.h"

class Plant: public PumpManagerDelegate {
  public:
    Plant();
    Plant(char* name, uint8_t pumpDigitalPin, uint8_t moistureAnalogPin, MessagePublisher *publisher);
    char* getName();
    void loop();
    void handlePumpStateMessage(uint8_t seconds);
    void handleRstWaterLvlMessage();
    void handlePumpTurnedOnEvent();
    void handlePumpTurnedOffEvent(unsigned long duration);
  private:
    void _sendWaterLevel();

    char _name[8] = { '\0' };
    PumpManager _pumpManager;
    WaterLevelStore _waterLevel;
    MoistureReader _moisture;
    MessagePublisher* _publisher;
    unsigned long _lastWaterLevelSentAt;
    unsigned long _lastMoistureSentAt;
    unsigned long _millisBefore;
};

#endif
