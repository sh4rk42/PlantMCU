#ifndef WaterLevelStore_h
#define WaterLevelStore_h

#include <Arduino.h>
#include "MessagePublisher.h"

class WaterLevelStore {
  public:
    WaterLevelStore();
    void setup(uint8_t pumpNumber);
    uint8_t getSeconds();
    void putSeconds(uint8_t seconds);
  private:
    uint8_t _pumpNumber;
};

#endif
