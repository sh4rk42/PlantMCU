#include <ESP8266WiFi.h>
#include "WiFiManager.h"
#include "ConfigManager.h"

WiFiManager::WiFiManager(ConfigManager* configManager) {
  _configManager = configManager;
}

void WiFiManager::connect() {
  char ssid[32], passphrase[32];
  bool ssidResult = _configManager->getChar(ssid, sizeof(ssid), F("WIFI_SSID"));
  bool passphraseResult = _configManager->getChar(passphrase, sizeof(passphrase), F("WIFI_PASS"));

  if(!ssidResult || !passphraseResult) {
    Serial.println(F("WiFiManager: unable to get WiFi network info from config"));
    return;
  }

  Serial.print(F("Connect to WiFi "));
  Serial.println(ssid);

  WiFi.begin(ssid, passphrase);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(F("."));
    delay(250);
  }
  Serial.println();
}
