#ifndef MoistureReader_h
#define MoistureReader_h

#include "ADC.h"

class MoistureReader {
  public:
    MoistureReader();
    void setup(uint8_t channel);
    float read();

  private:
    uint8_t _channel;
    ADC _adc;
};

#endif