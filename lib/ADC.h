#ifndef ADC_h
#define ADC_h

#include <Arduino.h>
#include <MCP3008.h>

#define PLANT_SENSOR_POWER_PIN D5

class ADC {
  public:
    ADC();
    int read(uint8_t chan);
};

#endif