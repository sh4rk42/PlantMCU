#include <Arduino.h>

#include "PumpAdapter.h"

PumpAdapter::PumpAdapter() {
}

void PumpAdapter::setup(uint8_t pinNumber) {
  _pinNumber = pinNumber;
  pinMode(_pinNumber, OUTPUT);
}

void PumpAdapter::turnOn() {
  digitalWrite(_pinNumber, HIGH);
}

void PumpAdapter::turnOff() {
  digitalWrite(_pinNumber, LOW);
}
