#ifndef PumpManagerDelegate_h
#define PumpManagerDelegate_h

#include <Arduino.h> # for byte

class PumpManagerDelegate {
  public:
    virtual void handlePumpTurnedOnEvent();
    virtual void handlePumpTurnedOffEvent(unsigned long duration);
};

#endif