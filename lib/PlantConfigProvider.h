#ifndef PlantConfigProvider_h
#define PlantConfigProvider_h

#include <stdint.h> # for uint8_t
#include "ConfigManager.h"
#include "PumpManager.h"
#include "Plant.h"
#include "MessagePublisher.h"

class PlantConfigProvider {
  public:
    PlantConfigProvider(ConfigManager *configManager);
    uint8_t getAll(Plant** buf, uint8_t size, MessagePublisher *publisher);

  private:
    ConfigManager *_configManager;
};

#endif
