#ifndef WiFiManager_h
#define WiFiManager_h

#include "ConfigManager.h"

class WiFiManager
{
  public:
    WiFiManager(ConfigManager* configManager);
    void connect();
  private:
    ConfigManager* _configManager;
};

#endif
