#include "ConfigManager.h"
#include "PlantConfigProvider.h"

PlantConfigProvider::PlantConfigProvider(ConfigManager* configManager) {
  _configManager = configManager;
}

uint8_t PlantConfigProvider::getAll(Plant** buf, uint8_t size, MessagePublisher *publisher) {
  char plantConfigStr[32];
  bool result = _configManager->getChar(plantConfigStr, sizeof(plantConfigStr), F("PLANTS"));
  if(!result) {
    return 0;
  }

  char *plantToken, *plantSaved;
  plantToken = strtok_r((char*)plantConfigStr, "\n", &plantSaved);
  uint8_t plantCount = 0;
  while(plantToken != NULL) {
    if(plantCount < size) {
      uint8_t fieldCount = 0;
      char *fieldToken, *fieldSaved;
      fieldToken = strtok_r(plantToken, ",", &fieldSaved);
      char name[8] = { '\0' };
      uint8_t pumpDigitalPin, moistureAnalogPin;
      while(fieldToken != NULL) {
        if(fieldCount == 0) {
          strncpy(name, fieldToken, 7);
        } else if(fieldCount == 1) {
          pumpDigitalPin = atoi(fieldToken);
        } else if(fieldCount == 2) {
          moistureAnalogPin = atoi(fieldToken);
        }

        fieldToken = strtok_r(NULL, ",", &fieldSaved);
        fieldCount += 1;
      }
      Plant* plant = new Plant(name, pumpDigitalPin, moistureAnalogPin, publisher);
      buf[plantCount] = plant;
    }

    plantToken = strtok_r(NULL, "\n", &plantSaved);
    plantCount += 1;
  }

  return plantCount;
}
