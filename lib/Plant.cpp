#include <string.h>
#include "Plant.h"
#include <Arduino.h>

Plant::Plant() {}

Plant::Plant(char* name, uint8_t pumpDigitalPin, uint8_t moistureAnalogPin, MessagePublisher *publisher) {
  strncpy(_name, name, 7);

  _pumpManager.setup(pumpDigitalPin, this);
  _waterLevel.setup(pumpDigitalPin);
  _moisture.setup(moistureAnalogPin);
  _publisher = publisher;
  _lastWaterLevelSentAt = 0;
  _lastMoistureSentAt = 0;
  _millisBefore = 0;
}

char* Plant::getName() {
  return _name;
}

void Plant::loop() {
  _pumpManager.loop();

  bool millisOverflow = millis() < _millisBefore;

  long timeSince = millis() - _lastWaterLevelSentAt;
  if(timeSince > 60 * 1000 || millisOverflow) {
    _sendWaterLevel();
    _lastWaterLevelSentAt = millis();
  }

  timeSince = millis() - _lastMoistureSentAt;
  if(timeSince > 60 * 1000 || millisOverflow) {
    char buf[7] = { '\0' };
    float moisture = _moisture.read();
    dtostrf(moisture * 100, 7, 1, buf);
    _publisher->publish(getName(), F("moisture"), buf);
    _lastMoistureSentAt = millis();
  }

  _millisBefore = millis();
}

void Plant::handlePumpStateMessage(uint8_t seconds) {
  _pumpManager.turnOn(seconds);
}

void Plant::handlePumpTurnedOnEvent() {
  Serial.print(F("Plant "));
  Serial.print(getName());
  Serial.println(F(" pump turned on"));

  _publisher->publish(getName(), F("pumpState"), "on");
}

void Plant::handlePumpTurnedOffEvent(unsigned long duration) {
  uint8_t durationInSeconds = duration / 1000;

  Serial.print(F("Plant "));
  Serial.print(getName());
  Serial.print(F(" pump turned off after "));
  Serial.println(durationInSeconds);

  _publisher->publish(getName(), F("pumpState"), "off");

  uint8_t waterLevelValue = _waterLevel.getSeconds();
  waterLevelValue += durationInSeconds;
  _waterLevel.putSeconds(waterLevelValue);
  _sendWaterLevel();
}

void Plant::_sendWaterLevel() {
  char buf[4];
  itoa(_waterLevel.getSeconds(), buf, 10);
  _publisher->publish(getName(), F("waterLevel"), buf);
}

void Plant::handleRstWaterLvlMessage() {
  _waterLevel.putSeconds(0);
}
