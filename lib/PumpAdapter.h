#ifndef PumpAdapter_h
#define PumpAdapter_h

#include <stdint.h> # for uint8_t

class PumpAdapter {
  public:
    PumpAdapter();
    void setup(uint8_t pinNumber);
    void turnOn();
    void turnOff();

  private:
    uint8_t _pinNumber;
};

#endif
