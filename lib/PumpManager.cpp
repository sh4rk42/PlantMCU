#include <Arduino.h>

#include "PumpManager.h"
#include "WaterLevelStore.h"

PumpManager::PumpManager() {
}

void PumpManager::setup(uint8_t pumpPin, PumpManagerDelegate* delegate) {
  _pump.setup(pumpPin);
  _turnedOnAt = 0;
  _turnOffAt = 0;
  _millisBefore = 0;
  _delegate = delegate;
}

void PumpManager::turnOn(uint8_t seconds) {
  _pump.turnOn();
  _turnedOnAt = millis();
  _turnOffAt = millis() + (seconds * 1000);
  _millisBefore = millis();
  _delegate->handlePumpTurnedOnEvent();
}

void PumpManager::loop() {
  if(_turnOffAt == 0) {
    return;
  }

  bool millisOverflow = millis() < _millisBefore;

  long timeUntilTurnOff = _turnOffAt - millis();
  if(timeUntilTurnOff < 0 || millisOverflow) {
    _pump.turnOff();
    _turnOffAt = 0;
    _delegate->handlePumpTurnedOffEvent(millis() - _turnedOnAt);
  }
}

PumpState PumpManager::state() {
  return _turnOffAt != 0;
}
