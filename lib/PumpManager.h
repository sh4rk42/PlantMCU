#ifndef PumpManager_h
#define PumpManager_h

#include <stdint.h> # for uint8_t
#include "PumpAdapter.h"
#include "MessagePublisher.h"
#include "PumpManagerDelegate.h"

typedef bool PumpState;
#define PUMP_STATE_ON true
#define PUMP_STATE_OFF false

class PumpManager {
  public:
    PumpManager();
    void setup(uint8_t pumpPin, PumpManagerDelegate* delegate);
    void turnOn(uint8_t seconds);
    void loop();
    PumpState state();

  private:
    unsigned long _turnedOnAt;
    unsigned long _turnOffAt;
    unsigned long _millisBefore;
    PumpAdapter _pump;
    PumpManagerDelegate* _delegate;
};

#endif
